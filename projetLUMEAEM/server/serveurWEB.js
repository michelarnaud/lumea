const express = require('express');
const http = require('http');
const cors = require('cors');
const sqlite3 = require('sqlite3').verbose();
const { ReadlineParser } = require('@serialport/parser-readline');
const { SerialPort } = require('serialport');
const { Server } = require('socket.io');

const PORT = process.env.PORT || 8082;

const app = express();
const server = http.createServer(app);
app.use((req, res, next) => {
    res.header('Cache-Control', 'no-cache, no-store, must-revalidate');
    res.header('Pragma', 'no-cache');
    res.header('Expires', '0');
    next();
});
const io = new Server(server, {
    cors: {
        origin: "*",
        methods: ["GET", "POST"]
    }
});

// Middleware
app.use(cors());
app.use(express.json());

// Selecting the serial port
const SP1 = new SerialPort({
    path: '/dev/ttyUSB0',
    baudRate: 115200
});

// Event on opening the SP1 port
SP1.on('open', function () {
    console.log('Serial port opened...');
});

// Creating the "newline" parser
const parser = new ReadlineParser();
// Piping SP1 stream to parser
SP1.pipe(parser);

// Connect to the database
//const db = new sqlite3.Database('serveurBDD.db', sqlite3.OPEN_READWRITE);

// Function to fetch Lumea data from the database
const fetchLumeaData = (callback) => {
    db.all('SELECT * FROM lumea ORDER BY Horodatage DESC LIMIT 10', (err, results) => {
        if (err) {
            console.error('Error executing SELECT query:', err);
            callback(err);
        } else {
            callback(null, results);
        }
    });
};

// Handling GET method for listing Lumea data
app.get('/', (req, res) => {
    fetchLumeaData((err, results) => {
        if (err) {
            console.error('Error fetching Lumea data:', err);
            return res.status(500).json({ error: 'Internal server error' });
        }
        res.json(results);
    });
});

// WebSocket connection handler
let dataListener = null; // Variable pour stocker le gestionnaire d'événement de réception de données
io.on('connection', (socket) => {
    console.log('Client connected');

    // WebSocket open event
    console.log("WebSocket server is open");

    // Assurez-vous de n'ajouter qu'un seul gestionnaire d'événement pour recevoir les données
    if (!dataListener) {
        dataListener = (data) => {
            console.log('Data received:', data);
            console.log('Sending data to clients'); // Log data to server console
            io.emit('lumeaData', data); // Send data to clients via WebSocket
        };
        parser.on('data', dataListener);
    }

    socket.on('disconnect', () => {
        console.log('Client disconnected');

        // Supprimer le gestionnaire d'événement de réception de données si plus aucun client n'est connecté
        if (io.engine.clientsCount === 0 && dataListener) {
            parser.off('data', dataListener);
            dataListener = null;
        }

        // WebSocket close event
        console.log("WebSocket server is closed");
    });
});

// Error handling middleware
app.use((err, req, res, next) => {
    console.error(err.stack);
    res.status(500).send({ error: 'Something went wrong!' });
});

server.listen(PORT, () => {
    console.log(`Server listening on port ${PORT}`);
});

// Close the database when the server shuts down
process.on('SIGINT', () => {
    db.close((err) => {
        if (err) {
            console.error('Error closing the database:', err);
            process.exit(1);
        }
        console.log('Database connection closed');
        process.exit(0);
    });
});
