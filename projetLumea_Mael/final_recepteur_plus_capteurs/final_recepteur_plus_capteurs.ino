#include <LoRa.h>
#include "boards.h"
#include <Wire.h>
#include <SPI.h>
#include <Adafruit_BMP280.h>
#include <Adafruit_INA219.h>
#include <ACS712.h>

#define BMP280_ADDRESS 0x76
Adafruit_BMP280 bmp;  // I2C
Adafruit_INA219 ina219_A;  // 0x40
Adafruit_INA219 ina219_B(0x41);
Adafruit_INA219 ina219_C(0x44);

//DEFINITION DES ENTREES ANALOGIQUES (MESURES DE TENSION + COURANT BATTERIE)
const int vPins[] = { 34, 35, 14, 12, 13, 15, 2};                              // définit les broches des CAN qu'on utilise
  int mesures[7];
int sortie = 4;                                                                     //Définition des variables correspondant aux broches de communication avec la Raspberry
int entree = 25;

void setup() {//---------SETUP : INITIALISATION, DEMARRAGE LORA ET ECRAN OLED, REPARATION A LA RECEPTION ------------------------------

  //ALLUMAGE DE LA CARTE
    initBoard();                                                                     //initialisation + délai nécéssaire
    delay(1500);

    for (int i = 0; i < 8; i++) {
    pinMode(vPins[i], INPUT);
    }
  
    Serial.println("~~~~~ RECEPTEUR LORA ~~~~~");
    delay(100);

    Serial.begin(115200);

      //DEMARRAGE DU LORA
      LoRa.setPins(RADIO_CS_PIN, RADIO_RST_PIN, RADIO_DIO0_PIN);                     // Démarre le Lora selon les broches définies par défaut dans les librairies
      Serial.println("Démarrage du LoRa...");

      //VERIFICATION DU DEMARRAGE LORA
      if (!LoRa.begin(LoRa_frequency)) {                                              //vérifie l'état de la comm. LoRa
        Serial.println("/!\\ ERREUR : Le récepteur Lora n'a pas pu être initialisé.");
        while (1);                                                                    // Si erreur, on interrompt le programme pour éviter tout dommage
      }
      Serial.println("OK");
      delay(1000);

      //VERIFICATION DU CAPTEUR DE TEMPERATURE
      Serial.println(F("BMP280 test"));
      unsigned status;
      status = bmp.begin(BMP280_ADDRESS);
      Serial.println("Connexion au BMP280...");
      if (!status) {
      Serial.println(F("Could not find a valid BMP280 sensor, check wiring or try a different address!"));
      Serial.print("SensorID was: 0x");
      Serial.println(bmp.sensorID(), 16);
      Serial.print("        ID of 0xFF probably means a bad address, a BMP 180 or BMP 085\n");
      Serial.print("ID of 0x56-0x58 represents a BMP 280,\n"
                   "ID of 0x60 represents a BME 280.\n"
                   "ID of 0x61 represents a BME 680.\n");
       while (1) delay(10);
      } else {
        Serial.println("Connexion au BMP280 OK");
      }

      // REGLAGES DU CAPTEUR DE TEMPERATURE
      bmp.setSampling(Adafruit_BMP280::MODE_NORMAL,     /* Operating Mode. */
                  Adafruit_BMP280::SAMPLING_X2,     /* Temp. oversampling */
                  Adafruit_BMP280::SAMPLING_X16,    /* Pressure oversampling */
                  Adafruit_BMP280::FILTER_X16,      /* Filtering. */
                  Adafruit_BMP280::STANDBY_MS_500); /* Standby time. */

      //VERIFICATION DES CAPTEURS DE COURANT
      if (!ina219_A.begin()) {
      Serial.println("Failed to find INA219 chip----1");
      while (1);
      }
      if (!ina219_B.begin()) {
      Serial.println("Failed to find INA219 chip----2");
      while (1);
      }
      if (!ina219_C.begin()) {
      Serial.println("Failed to find INA219 chip----3");
      while (1);
      }

     //PREPARATION A LA RECEPTION D'UN PAQUET
     Serial.println("En attente d'un ordre d'allumage...");
}

void loop() { //----------------------------LOOP : RECEPTION DE DONNEES, BIT ACK ET RETOUR CONFIRMATION---------------------------

  unsigned long hourLoop = millis();
       while (millis() - hourLoop < 10000) { //PASSER A 3600000

         //RECEPTION D'UN PAQUET
        int packetReceived = LoRa.parsePacket();                                               // A chaque boucle, on vérifie la reception d'un paquet LoRa via la variable
       if(packetReceived){
       Serial.println("!!! Ordre d'allumage recu");

        //ENVOI DU BIT ACK
       Serial.println("Transmission à la carte Raspberry...");
       pinMode(sortie, OUTPUT);                                                            // On définie l'utilisation des broches en tant qu'entrées ou sorties
       pinMode(entree, INPUT);
       digitalWrite(sortie, HIGH);                                                         // Broche sortie high pdt. 1.5s , c'est le bit ACK qu'on crée et envoie à la Raspberry
       delay(1500);
       digitalWrite(sortie, LOW);
       delay(3000);
    
       //ATTENTE DU RETOUR
       Serial.println("En attente de confirmation...");

       //RECEPTION DU RETOUR
       unsigned long startTime = millis();
       while (millis() - startTime < 5000) {                                              // On attend une réponse pendant 5 secondes max
         if (digitalRead(entree) == HIGH) {                                               // Permet de lire la broche d'entrée
           Serial.println("!!! Retour recu");

           Serial.println("Envoi de la confirmation ACK à l'émetteur...");
           LoRa.beginPacket();
           LoRa.print("CONFIRM_WIFI_ON");                                                     //envoi de la confirmation à l'émetteur
           LoRa.endPacket();
          break;                                                                         //si le bit ACK et re-recu avant les 5 secondes, on interrompt la boucle d'attente
         } else {
           Serial.println("/!\\ ERREUR : Pas de confirmation recue.");
           delay(5000);
         }
        }
      }
       }

  //MESURES DES COURANTS
  float currentBatterie = 0;
  float totCurrentBatterie = 0;
    for (int x = 0; x < 10; x++){
    currentBatterie = (analogRead(36)-2495)*1000/110;
    totCurrentBatterie = totCurrentBatterie + currentBatterie;
  delay (2);
}
currentBatterie = totCurrentBatterie / 10;                       // En raison de l'imprecision du capteur, nous effectuons 10 mesures puis une moyenne pour trouver une valeur plus précise
  
  float current_mA = 0;
  float current_mA_ = 0;
  float current_mA_C = 0;
  current_mA = ina219_A.getCurrent_mA();
  current_mA_ = ina219_B.getCurrent_mA();
  current_mA_C = ina219_C.getCurrent_mA();

  //MESURES DES TENSIONS
  for (int i = 0; i < 7; i++) {
      mesures[i] = analogRead(vPins[i]);  //On fait les mesures et les stockons dans le tableau de valeur
    }

  //CALCUL DES PUISSANCES ET AUTRES MESURES
  float PuissanceBatterie = (currentBatterie / 1000) * (analogRead(vPins[0]) * 12 / 4096);
  float PuissanceEolienne = (current_mA / 1000) * (analogRead(vPins[4]) * 12 / 4096);
  float PuissancePV1 = (current_mA_ / 1000) * (analogRead(vPins[1]) * 12 / 4096);
  float PuissancePV2 = (current_mA_C / 1000) * (analogRead(vPins[5]) * 12 / 4096);
  float PuissancePV = PuissancePV1 + PuissancePV2;
  int Horodatage = 0;
  int EtatEclairage = 0; 
  float tensionBatterie = (analogRead(vPins[0])) * 12 / 4096;

  //ENVOI DES MESURES PAR JSON
  Serial.print("{");
  Serial.print("\"ID_Mat\":\"");
  Serial.print("1");  // A remplacer par vrai ID mat ?
  Serial.print("\",\"TemperatureBatterie\":\"");
  Serial.print(bmp.readTemperature());
  Serial.print("\",\"IntensiteBatterie\":\"");
  Serial.print(currentBatterie);  // A remplacer par capteur courant batterie
  Serial.print("\",\"TensionBatterie\":\"");
  Serial.print(tensionBatterie);
  Serial.print("\",\"EnergieConsommee\":\"");
  Serial.print(PuissanceBatterie);
  Serial.print("\",\"ChargeBatterie\":\"");
  Serial.print(((tensionBatterie * 7 * 0.99)/currentBatterie)*100);  // A remplacer par vraie charge batterie
  Serial.print("\",\"IntensiteEolienne\":\"");
  Serial.print(current_mA);
  Serial.print("\",\"TensionEolienne\":\"");
  Serial.print((analogRead(vPins[4])) * 12 / 4096);
  Serial.print("\",\"EnergieProduiteEolienne\":\"");
  Serial.print(PuissanceEolienne);
  Serial.print("\",\"IntensitePV1\":\"");
  Serial.print(current_mA_);
  Serial.print("\",\"TensionPV1\":\"");
  Serial.print((analogRead(vPins[1])) * 12 / 4096);
  Serial.print("\",\"IntensitePV2\":\"");
  Serial.print(current_mA_C);
  Serial.print("\",\"TensionPV2\":\"");
  Serial.print((analogRead(vPins[5])) * 12 / 4096);
  Serial.print("\",\"EnergieProduitePV\":\"");
  Serial.print(PuissancePV);
  Serial.print("\",\"EtatEclairage\":\"");
  Serial.print(EtatEclairage);
  Serial.print("\",\"Horodatage\":\"");
  Serial.print(Horodatage);
  Serial.println("\"}");
}
