#include <LoRa.h>
#include "boards.h"

int sortie = 15;                                                                     //Définition des variables correspondant aux broches de communication avec la Raspberry
int entree = 2;

void setup() {//---------SETUP : INITIALISATION, DEMARRAGE LORA ET ECRAN OLED, REPARATION A LA RECEPTION ------------------------------
  
  //ALLUMAGE DE LA CARTE
    initBoard();                                                                     //initialisation + délai nécéssaire
    delay(1500);
    pinMode(sortie, OUTPUT);                                                            // On définie l'utilisation des broches en tant qu'entrées ou sorties
    pinMode(entree, INPUT);

  #ifdef HAS_DISPLAY
  
    Serial.println("~~~~~ RECEPTEUR LORA ~~~~~");
    
    //INITIALISATION
    Serial.println("Démarrage de l'écran OLED...");
    if (u8g2) {                                                                      //vérifie l'activation de l'écran
      char buf[256];
      u8g2->clearBuffer();
      u8g2->drawStr(0, 12, "RECEPTEUR LORA");                                         //initialisation de l'écran + affichage si OK
      u8g2->sendBuffer();
      delay(3000);
      Serial.println("OK");

      //DEMARRAGE DU LORA
      LoRa.setPins(RADIO_CS_PIN, RADIO_RST_PIN, RADIO_DIO0_PIN);                     // Démarre le Lora selon les broches définies par défaut dans les librairies
      Serial.println("Démarrage du LoRa...");
      u8g2->drawStr(0, 24, "Allumage LoRa...");
      u8g2->sendBuffer();

      //VERIFICATION DU DEMARRAGE LORA
      if (!LoRa.begin(LoRa_frequency)) {                                              //vérifie l'état de la comm. LoRa
        Serial.println("/!\\ ERREUR : Le récepteur Lora n'a pas pu être initialisé.");
        while (1);                                                                    // Si erreur, on interrompt le programme pour éviter tout dommage
      }
      Serial.println("OK");
      u8g2->drawStr(0, 36, "OK");
      u8g2->sendBuffer();
      delay(1000);

     //PREPARATION A LA RECEPTION D'UN PAQUET
     Serial.println("En attente d'un ordre d'allumage...");
     u8g2->clearBuffer();
     u8g2->drawStr(0, 12, "En attente...");
     u8g2->sendBuffer();

    } else {
     Serial.println("/!\\ ERREUR : L'écran OLED n'a pas pu être initialisé.");
     while(1);                                                                            // Si erreur d'écran, on interrompt le programme pour éviter tout dommage
    }
  #endif

}

void loop() { //----------------------------LOOP : RECEPTION DE DONNEES, BIT ACK ET RETOUR CONFIRMATION---------------------------

  #ifdef HAS_DISPLAY

   if(u8g2){
     //RECEPTION D'UN PAQUET
     int packetReceived = LoRa.parsePacket();                                               // A chaque boucle, on vérifie la reception d'un paquet LoRa via la variable
     if(packetReceived){
       Serial.println("!!! Ordre d'allumage recu");
       u8g2->clearBuffer();
       u8g2->drawStr(0, 24, "!!!");
       u8g2->drawStr(0, 36, "Envoi en cours...");
       u8g2->sendBuffer(); 

        //ENVOI DU BIT ACK
       Serial.println("Transmission à la carte Raspberry...");
       digitalWrite(sortie, HIGH);                                                         // Broche sortie high pdt. 1.5s , c'est le bit ACK qu'on crée et envoie à la Raspberry
       delay(1500);
       digitalWrite(sortie, LOW);
       delay(3000);
    
       //ATTENTE DU RETOUR
       u8g2->clearBuffer();
       u8g2->drawStr(0, 12, "Attente de confirm.");
       u8g2->sendBuffer();
       Serial.println("En attente de confirmation...");

       //RECEPTION DU RETOUR
       unsigned long startTime = millis();
       while (millis() - startTime < 5000) {                                              // On attend une réponse pendant 5 secondes max
         if (digitalRead(entree) == HIGH) {                                               // Permet de lire la broche d'entrée
           Serial.println("!!! Retour recu");
           u8g2->drawStr(0, 24, "!!!");
           u8g2->drawStr(0, 36, "confirm. recue");
           u8g2->sendBuffer();

           Serial.println("Envoi de la confirmation ACK à l'émetteur...");
           LoRa.beginPacket();
           LoRa.print("CONFIRM_WIFI_ON");                                                     //envoi de la confirmation à l'émetteur
           LoRa.endPacket();
          break;                                                                         //si le bit ACK et re-recu avant les 5 secondes, on interrompt la boucle d'attente
         } else {
           Serial.println("/!\\ ERREUR : Pas de confirmation recue.");
           u8g2->drawStr(0, 24, "/!\\ Retour ERREUR");
           u8g2->sendBuffer();
           delay(5000);
         }
        }
      }
    }
  #endif
}
