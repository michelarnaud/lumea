#include <LoRa.h>
#include "boards.h"

int counter = 0;
bool erreur = false;                                                                 //création des variables

void setup()
{
    //ALLUMAGE DE LA CARTE
    initBoard();                                                                     //initialisation + délai nécéssaire
    delay(1500);

  #ifdef HAS_DISPLAY
  
    Serial.println("~~~~~ EMETTEUR LORA ~~~~~");
    
    //INITIALISATION
    Serial.println("Démarrage de l'écran OLED...");
    if (u8g2) {                                                                      //vérifie l'activation de l'écran
      char buf[256];
      u8g2->clearBuffer();
      u8g2->drawStr(0, 12, "EMETTEUR LORA");                                         //initialisation de l'écran + affichage si OK
      u8g2->sendBuffer();
      delay(3000);
      Serial.println("OK");

      //DEMARRAGE DU LORA
      LoRa.setPins(RADIO_CS_PIN, RADIO_RST_PIN, RADIO_DIO0_PIN);                     // Démarre le Lora selon les broches définies par défaut dans les librairies
      Serial.println("Démarrage du LoRa...");
      u8g2->drawStr(0, 24, "Allumage LoRa...");
      u8g2->sendBuffer();

      //VERIFICATION DU DEMARRAGE LORA
      if (!LoRa.begin(LoRa_frequency)) {                                              //vérifie l'état de la comm. LoRa
        Serial.println("/!\\ ERREUR : L'émetteur Lora n'a pas pu être initialisé.");
        while (1);                                                                    // Si erreur, on interrompt le programme pour éviter tout dommage
      }
      Serial.println("OK");
      u8g2->drawStr(0, 36, "OK");
      u8g2->sendBuffer();
      delay(1000);

    //ENVOI DU PACKET D'ALLUMAGE WI-FI PAR LORA
    Serial.println("Envoi de l'ordre d'allumage en cours...");
    u8g2->clearBuffer();
    u8g2->drawStr(0, 12, "Envoi en cours...");
    u8g2->sendBuffer();
    LoRa.beginPacket();
    LoRa.print("REQUEST_WIFI_ON");                                                     //envoi du paquet LoRa au recepteur
    LoRa.endPacket();

    //ATTENTE DU PACKET RETOUR DE CONFIRMATION
    Serial.println("OK");
    Serial.println("En attente de confirmation...");
    unsigned long startTime = millis();
    bool packetReceived = false;
    while (millis() - startTime < 5000) {                                              //On attend une réponse pendant 5 secondes max
      if (LoRa.parsePacket()) {
         packetReceived = true;
        break;                                                                         //si un paquet est recu avant les 5 secondes, on interrompt la boucle d'attente
      }
    }

    //VERIFICATION DE LA RECEPTION DU BIT ACK
    if(packetReceived){                                                                 //Vérifie si le bit de retour ACK est recu
      u8g2->clearBuffer();
      u8g2->drawStr(0, 12, "Wi-Fi ON");
      u8g2->sendBuffer();
      Serial.println("OK");
      delay(2000);
    } else {
      u8g2->clearBuffer();
      u8g2->drawStr(0, 12, "/!\\ Erreur");
      u8g2->drawStr(0, 30, "Veuillez reessayer.");
      u8g2->sendBuffer();
      Serial.println("/!\\ ERREUR : Wi-Fi non allumé ou pas de confirmation recue");
      erreur = true;                                                                    //A basculer sur "false" pour les tests, sur "true" pour le prog. final
      delay(3000);
    }

  } else {
    Serial.println("/!\\ ERREUR : L'écran OLED n'a pas pu être initialisé.");
    while(1);                                                                            // Si erreur d'écran, on interrompt le programme pour éviter tout dommage
  }
  #endif
}

void loop()
{
  #ifdef HAS_DISPLAY                                                                    // On redéfinit l'écran
  if (u8g2) {                                                                           // Vérifie l'état de l'écran
    char buf[256];

    if(erreur == true){
        while(1);
    }

      u8g2->clearBuffer();
      u8g2->drawStr(0, 24, "Pour redemarrer le");
      u8g2->drawStr(0, 36, "WiFi : Debranchez et");
      u8g2->drawStr(0, 48, "rebranchez ou pressez");
      u8g2->drawStr(0, 60, "le bouton RESET.");
      u8g2->sendBuffer();

  }
#endif
}