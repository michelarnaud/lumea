#include <Wire.h>  // On inclue les bibliothèques nécessaires
#include <SPI.h>
#include <Adafruit_BMP280.h>
#include <Adafruit_INA219.h>
#include <ACS712.h>
#define BMP280_ADDRESS 0x76
Adafruit_BMP280 bmp;  // I2C

Adafruit_INA219 ina219_A;  // 0x40
Adafruit_INA219 ina219_B(0x41);
Adafruit_INA219 ina219_C(0x44);

int ID_Mat = 0;

const int vPins[] = { 34, 35, 32, 33, 25, 26, 27, 14 };  // définit les broches des CAN qu'on utilise
int mesures[2][4];

void setup(void) {
  pinMode(15, INPUT);
  for (int i = 0; i < 8; i++) {
    pinMode(vPins[i], INPUT);  //on les règle en INPUT (entrée)
  }
 Serial.begin(115200);  //on ouvre la communication série

  while (!Serial)
    ;
  Serial.println(F("BMP280 test"));
  unsigned status;
  status = bmp.begin(BMP280_ADDRESS);

  Serial.println("Connexion au BMP280...");

  if (!status) {
    Serial.println(F("Could not find a valid BMP280 sensor, check wiring or try a different address!"));
    Serial.print("SensorID was: 0x");
    Serial.println(bmp.sensorID(), 16);
    Serial.print("        ID of 0xFF probably means a bad address, a BMP 180 or BMP 085\n");
    Serial.print("ID of 0x56-0x58 represents a BMP 280,\n"
                 "ID of 0x60 represents a BME 280.\n"
                 "ID of 0x61 represents a BME 680.\n");
    while (1) delay(10);
  } else {
    Serial.println("Connexion au BMP280 OK");
  }

  if (!ina219_A.begin()) {
    Serial.println("Failed to find INA219 chip----1");
    while (1) { delay(10); }
  }
  if (!ina219_B.begin()) {
    Serial.println("Failed to find INA219 chip----2");
    while (1) { delay(10); }
  }

  if (!ina219_C.begin()) {
    Serial.println("Failed to find INA219 chip----3");
    while (1) { delay(10); }
  }

  bmp.setSampling(Adafruit_BMP280::MODE_NORMAL,     /* Operating Mode. */
                  Adafruit_BMP280::SAMPLING_X2,     /* Temp. oversampling */
                  Adafruit_BMP280::SAMPLING_X16,    /* Pressure oversampling */
                  Adafruit_BMP280::FILTER_X16,      /* Filtering. */
                  Adafruit_BMP280::STANDBY_MS_500); /* Standby time. */
}

void loop() {

  float currentBatterie = 0;
  float totCurrentBatterie = 0;
    for (int x = 0; x < 10; x++){
    currentBatterie = (analogRead(36)-2495)*1000/110;
    totCurrentBatterie = totCurrentBatterie + currentBatterie;
  delay (2);
}
currentBatterie = totCurrentBatterie / 10;                       // En raison de l'imprecision du capteur, nous effectuons 10 mesures puis une moyenne pour trouver une valeur plus précise
  
  float current_mA = 0;
  float current_mA_ = 0;
  float current_mA_C = 0;

  for (int i = 0; i < 2; i++) {
    for (int j = 0; j < 4; j++) {
      mesures[i][j] = analogRead(vPins[i * 4 + j]);  //On fait les mesures et les stockons dans le tableau de valeur
    }
  }

  current_mA = ina219_A.getCurrent_mA();
  current_mA_ = ina219_B.getCurrent_mA();
  current_mA_C = ina219_C.getCurrent_mA();

  float PuissanceBatterie = (currentBatterie / 1000) * (analogRead(vPins[0]) * 12 / 4096);
  float PuissanceEolienne = (current_mA / 1000) * (analogRead(vPins[4]) * 12 / 4096);
  float PuissancePV1 = (current_mA_ / 1000) * (analogRead(vPins[1]) * 12 / 4096);
  float PuissancePV2 = (current_mA_C / 1000) * (analogRead(vPins[5]) * 12 / 4096);
  float PuissancePV = PuissancePV1 + PuissancePV2;
  int Horodatage = 0;

  int EtatEclairage = 0;

  Serial.print("{");
  Serial.print("\"ID_Mat\":\"");
  Serial.print("1");  // A remplacer par vrai ID mat ?
  Serial.print("\",\"TemperatureBatterie\":\"");
  Serial.print(bmp.readTemperature());
  Serial.print("\",\"IntensiteBatterie\":\"");
  Serial.print(currentBatterie);  // A remplacer par capteur courant batterie
  Serial.print("\",\"TensionBatterie\":\"");
  Serial.print((analogRead(vPins[0])) * 12 / 4096);
  Serial.print("\",\"EnergieConsommee\":\"");
  Serial.print(PuissanceBatterie);
  Serial.print("\",\"ChargeBatterie\":\"");
  Serial.print("0");  // A remplacer par vraie charge batterie
  Serial.print("\",\"IntensiteEolienne\":\"");
  Serial.print(current_mA);
  Serial.print("\",\"TensionEolienne\":\"");
  Serial.print((analogRead(vPins[4])) * 12 / 4096);
  Serial.print("\",\"EnergieProduiteEolienne\":\"");
  Serial.print(PuissanceEolienne);
  Serial.print("\",\"IntensitePV1\":\"");
  Serial.print(current_mA_);
  Serial.print("\",\"TensionPV1\":\"");
  Serial.print((analogRead(vPins[1])) * 12 / 4096);
  Serial.print("\",\"IntensitePV2\":\"");
  Serial.print(current_mA_C);
  Serial.print("\",\"TensionPV2\":\"");
  Serial.print((analogRead(vPins[5])) * 12 / 4096);
  Serial.print("\",\"EnergieProduitePV\":\"");
  Serial.print(PuissancePV);
  Serial.print("\",\"EtatEclairage\":\"");
  Serial.print(EtatEclairage);
  Serial.print("\",\"Horodatage\":\"");
  Serial.print(Horodatage);
  Serial.println("\"}");

  // Serial.println("#############################################################################");
  delay(1000);  //On attend 1s avant le prohain échantillon
}